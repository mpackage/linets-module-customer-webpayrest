<?php

namespace Linets\CustomerWebpayRest\Model\Config;


class ConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    const SECURITY_CONFIGS_ROUTE = 'payment/transbank_webpay/security/';
    const ORDER_CONFIGS_ROUTE = 'payment/transbank_webpay/general_parameters/';

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface)
    {
        $this->scopeConfigInterface = $scopeConfigInterface;
    }

    public function getConfig()
    {
        return [
            'pluginConfigWebpay' => [
                'createTransactionUrl' => 'transaction/createwebpay',
            ],
        ];
    }

    public function getPluginConfig()
    {
        /*Inicio Modificacion Linets para evitar la perdida del quote al comprar ok */
        $config = [
            'ENVIRONMENT'               => $this->scopeConfigInterface->getValue(self::SECURITY_CONFIGS_ROUTE.'environment', 'websites'),
            'COMMERCE_CODE'             => $this->scopeConfigInterface->getValue(self::SECURITY_CONFIGS_ROUTE.'commerce_code', 'websites'),
            'API_KEY'                   => $this->scopeConfigInterface->getValue(self::SECURITY_CONFIGS_ROUTE.'api_key', 'websites'),
            'URL_RETURN'                => 'checkout/transaction/commitwebpay',
            'ECOMMERCE'                 => 'magento',
            'new_order_status'          => $this->getOrderPendingStatus(),
            'payment_successful_status' => $this->getOrderSuccessStatus(),
            'payment_error_status'      => $this->getOrderErrorStatus(),
        ];
        /*Fin Modificacion Linets para evitar la perdida del quote al comprar ok */

        return $config;
    }
    /*Inicio Modificacion Linets para evitar la perdida del quote al comprar ok */

    public function getOrderPendingStatus()
    {
        return $this->scopeConfigInterface->getValue(self::ORDER_CONFIGS_ROUTE.'new_order_status','websites');
    }

    public function getOrderSuccessStatus()
    {
        return $this->scopeConfigInterface->getValue(self::ORDER_CONFIGS_ROUTE.'payment_successful_status','websites');
    }

    public function getOrderErrorStatus()
    {
        return $this->scopeConfigInterface->getValue(self::ORDER_CONFIGS_ROUTE.'payment_error_status','websites');
    }
    /*Fin Modificacion Linets para evitar la perdida del quote al comprar ok */

}
