<?php

namespace Linets\CustomerWebpayRest\Block\OnePage;

use Transbank\Webpay\Model\WebpayOrderDataFactory;

class Success extends \Magento\Checkout\Block\Onepage\Success
{
    /**
     * @var \Transbank\Webpay\Model\WebpayOrderDataFactory
     */
    protected $webpayOrderDataFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Transbank\Webpay\Model\WebpayOrderDataFactory $webpayOrderDataFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        WebpayOrderDataFactory $webpayOrderDataFactory,
        array $data = []
    ) {
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);
        $this->webpayOrderDataFactory = $webpayOrderDataFactory;
    }

    public function getSuccessMessage()
    {
        $order = $this->_checkoutSession->getLastRealOrder();
        $webpayOrderDataModel = $this->webpayOrderDataFactory->create();
        $webpayOrderData = $webpayOrderDataModel->load($order->getIncrementId(), 'order_id');
        $data = $this->buildSuccessData($webpayOrderData);
        return $data;
    }

    private function buildSuccessData($webpayOrderData)
    {
        $data = [];
        $transactionResult = json_decode($webpayOrderData->getMetadata(), true);

        if(isset($transactionResult['responseCode'])) {

            /**
             * 1 or more than 1 product
             */
            $cont = 0;
            $data['order_id'] =  $webpayOrderData->getOrderId();
            $data['token'] =  $webpayOrderData->getToken();
            $data['payment_status'] =  $webpayOrderData->getPaymentStatus();

            if (
                $transactionResult['paymentTypeCode'] == "SI" ||
                $transactionResult['paymentTypeCode'] == "S2" ||
                $transactionResult['paymentTypeCode'] == "NC" ||
                $transactionResult['paymentTypeCode'] == "VC") {
                $tipoCuotas = $transactionResult['paymentTypeCode'];
            } else {
                $tipoCuotas = "Sin cuotas";
            }

            if ($transactionResult['responseCode'] == 0) {
                $transactionResponse =  "Transacci&oacute;n Aprobada";
            } else {
                $transactionResponse = "Transacci&oacute;n Rechazada";
            }

            if ($transactionResult['paymentTypeCode'] == "VD") {
                $paymentType = "Débito";
            } else {
                $paymentType = "Crédito";
            }

            if (isset($transactionResult['commerceCode'])){
                $data['details'][$cont]['commerce_code'] =  $transactionResult['commerceCode'];
            }
            $data['details'][$cont]['transaction_reponse'] =  $transactionResponse;
            $data['details'][$cont]['response_code'] =  $transactionResult['responseCode'];
            $data['details'][$cont]['amount'] = $transactionResult['amount'];
            $data['details'][$cont]['buy_order'] = $transactionResult['buyOrder'];
            $data['details'][$cont]['transaction_date'] = date('d-m-Y', strtotime($transactionResult['transactionDate']));
            $data['details'][$cont]['transaction_hour'] = date('H:i:s', strtotime($transactionResult['transactionDate']));
            if (isset($transactionResult['cardDetail']['card_number'])){
                $data['details'][$cont]['card'] =  "**** **** **** " . $transactionResult['cardDetail']['card_number'];
            }

            $data['details'][$cont]['authorization_code'] = $transactionResult['authorizationCode'];

            $data['details'][$cont]['payment_type'] = $paymentType;
            $data['details'][$cont]['instalments_type'] = $tipoCuotas;
            if (isset($transactionResult['installmentsNumber'])){
                $data['details'][$cont]['installments_number'] = $transactionResult['installmentsNumber'];
            }
            if (isset($transactionResult['installmentsAmount'])){
                $data['details'][$cont]['installments_amount'] = $transactionResult['installmentsAmount'];
            }
        }
        return $data;
    }
}
